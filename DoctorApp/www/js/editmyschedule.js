function init() {
        var appoint_id=localStorage.getItem("edit_appoint_id1");
        $(".loading-page").show();
        $.ajax({
                url: BASE_URL,
                type: "POST",
                dataType: "json",
                data: {appoint_id:appoint_id,action: 'return_edit_myschedule'},
                crossDomain: true
            }).done(function (data) {
            $(".loading-page").hide();
                if(data.response.id<=0)
                {
                    alert(data.response.message);
                }
                else
                {
                     document.getElementById("patient_name").innerHTML =data.response.fname;
                     document.getElementById("note_to_patient").innerHTML =data.response.note_on_patient;
                     var date_time=data.response.proposed_time;
                     var pro_day=data.response.proposed_day;
                     if(pro_day!="")
                     {
                         var hours=date_time.substring(0, 2);
                         var minutes=date_time.substring(3, 5);
                         var dayam=date_time.substring(6, 8);
                         var dayarr = new Array();
                         dayarr[0] = "Tommorow";
                         dayarr[1] = "Today";
                         var days='<select id="day1">';
                         days=days+'<option>DD</option>';
                         for(var i=0;i<2;i++)
                         {
                              if(dayarr[i]==pro_day)
                              {
                                days=days+'<option selected>'+dayarr[i]+'</option>';
                              }
                              else
                              {
                                days=days+'<option>'+dayarr[i]+'</option>';
                              }
                         }
                         days+='</select>';
                       document.getElementById("getday").innerHTML=days;


                        var timearr = new Array();
                        timearr[1] = "01";
                        timearr[2] = "02";
                        timearr[3] = "03";
                        timearr[4] = "04";
                        timearr[5] = "05";
                        timearr[6] = "05";
                        timearr[7] = "07";
                        timearr[8] = "08";
                        timearr[9] = "09";
                        timearr[10] = "10";
                        timearr[11] = "11";
                        timearr[11] = "12";
                         var hrs='<select id="hr1">';
                         hrs=hrs+'<option>HH</option>';
                         for(i=1;i<12;i++)
                         {
                              if(timearr[i]==hours)
                              {
                                hrs+='<option selected>'+timearr[i]+'</option>';
                              }
                              else
                              {
                                 hrs+='<option>'+timearr[i]+'</option>';
                              }
                         }
                         hrs+='</select>';
                       document.getElementById("gethrs").innerHTML=hrs;


                          var minarr = new Array();
                          minarr[1] = "00";
                          minarr[2] = "15";
                          minarr[3] = "30";
                          minarr[4] = "45";
                         var mins='<select id="min1">';
                         mins=mins+'<option>MM</option>';
                         for(j=1;j<=4;j++)
                         {
                             if(minarr[j]==minutes)
                             {
                                 mins=mins+'<option selected>'+minarr[j]+'</option>';
                             }
                             else
                             {
                                mins=mins+'<option>'+minarr[j]+'</option>';
                             }
                         }
                         mins+="</select>";
                       document.getElementById("getmins").innerHTML=mins;


                         var waqt = new Array();
                         waqt[1] = "AM";
                         waqt[2] = "PM";
                         var pm='<select id="am1">';
                         for(i=1;i<=2;i++)
                         {
                            if(waqt[i]==dayam)
                            {
                                pm+='<option selected>'+waqt[i]+'</option>';
                            }
                            else
                            {
                                pm+='<option>'+waqt[i]+'</option>';
                            }
                         }
                         pm+='</select>';
                        document.getElementById("getam").innerHTML=pm;
                         window.localStorage.setItem("check_day1",'');

                    }
                    else
                    {
                          var minarr = new Array();
                           minarr[1] = "00 Mins";
                           minarr[2] = "15 Mins";
                           minarr[3] = "30 Mins";
                           minarr[4] = "45 Mins";
                          var mins='<select id="mins_sel">';
                          mins=mins+'<option>MM</option>';
                          for(j=1;j<=4;j++)
                          {
                              if(minarr[j]==date_time)
                              {
                                  mins=mins+'<option selected>'+minarr[j]+'</option>';
                              }
                              else
                              {
                                 mins=mins+'<option>'+minarr[j]+'</option>';
                              }
                          }
                          mins+="</select>";
                          document.getElementById("getmins").innerHTML=mins;
                           window.localStorage.setItem("check_day1",'asap');
                    }

                }

            }).fail(function (reason) {
            $(".loading-page").hide();
                console.log("Error: "+JSON.stringify(reason));

            });

}

function dashboard(id)
{
    if(id=='dash')
        window.location="doctor_dashboard.html";
    else
        window.location="my-schedule.html";
}
function update_proposal()
{
    var mins_check=localStorage.getItem("check_day1");
    if(mins_check=='')
    {
        var day1=document.getElementById('day1').value;
        var hr1=document.getElementById('hr1').value;
        var min1=document.getElementById('min1').value;
        var am1=document.getElementById('am1').value;
        if(day1=='DD')
        {
            alert("Please select day");
            return false;
        }else if(hr1=='HH')
        {
            alert("Please select hours");
            return false;
        }else if(min1=='MM')
        {
            alert("Please select minutes");
            return false;
        }

    }
    else
    {
        var mins_sel=document.getElementById('mins_sel').value;
        if(mins_sel=='MM')
        {
                alert("Please select minutes");
                return false;
        }
        var final_day=mins_sel;
        var day1='';
    }
    var note_to_patient=document.getElementById('note_to_patient').value;
    if(note_to_patient=="")
    {
        alert("Enter note to patient");
        return false;
    }
    else
    {
        $(".loading-page").show();
        var appoint_id=localStorage.getItem("edit_appoint_id1");
        if(mins_check=='')
        var final_day=hr1+" "+min1+" "+am1;
        $.ajax({
            url: BASE_URL,
            type: "POST",
            dataType: "json",
            data: {action:'edit_doctor_schedule',appoint_id:appoint_id,day:day1,final_day:final_day,note_to_patient:note_to_patient},
            crossDomain: true
        }).done(function (data) {
        $(".loading-page").hide();
            if(data.response.id<=0)
                alert(data.response.message);
            else
            {
                 alert(data.response.message);
                 window.location="my-schedule.html";
             }

        }).fail(function (reason) {
        $(".loading-page").hide();
            console.log("Error: "+JSON.stringify(reason));

        });
    }
}
function cancel_proposal()
{
    var appoint_id=localStorage.getItem("edit_appoint_id1");
    $(".loading-page").show();
    $.ajax({
        url: BASE_URL,
        type: "POST",
        dataType: "json",
        data: {action:'cancel_doctor_schedule',appoint_id:appoint_id},
        crossDomain: true
    }).done(function (data) {
    $(".loading-page").hide();
        if(data.response.id<=0)
            alert(data.response.message);
        else
        {
             alert(data.response.message);
             window.location="my-schedule.html";
         }

    }).fail(function (reason) {
        console.log("Error: "+JSON.stringify(reason));

    });
}
