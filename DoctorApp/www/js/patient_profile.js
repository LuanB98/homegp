function init() {
        var appointid=localStorage.getItem("appointid1");
         $(".loading-page").show();
        $.ajax({
            url: BASE_URL,
            type: "POST",
            dataType: "json",
            data: {appointid:appointid,action: 'return_patient_profile_for_doc'},
            crossDomain: true
        }).done(function (data) {
         $(".loading-page").hide();
            if(data.response.id<=0)
            {
                alert(data.response.message);
            }
            else
            {

                  document.getElementById("quick_desc").value = data.response.quick_desc;
                  document.getElementById("skills_require").innerHTML = data.response.skill_require;
                  document.getElementById("full_name").innerHTML = data.response.fname+" "+data.response.lname;
                  document.getElementById("note_on_patient").value = data.response.fname+" "+data.response.note_on_patient;
                  document.getElementById("street_address").innerHTML = data.response.street_no+" "+data.response.street_name+","+data.response.suburb;
            }

        }).fail(function (reason) {
         $(".loading-page").hide();
            console.log("Error: "+JSON.stringify(reason));

        });


}
function dashboard()
{
    window.location="doctor_dashboard.html";
}