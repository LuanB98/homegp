function init() {

       var doc_id=localStorage.getItem("user_id1");
       $(".loading-page").show();
       window.localStorage.removeItem("appoint_id1");

       var suburb=localStorage.getItem("suburb");
       if(suburb===null)
       {
            $.ajax({
                 url: BASE_URL,
                 type: "POST",
                 dataType: "json",
                 data: {doctor_id:doc_id,action: 'return_favourite_list'},
                 crossDomain: true
             }).done(function (data) {

                 $(".loading-page").hide();
                 suburb=data.response.fav_suburbs;
                 document.getElementById('sub').innerHTML=suburb;
                 getdata(suburb,doc_id);

             }).fail(function (reason) {
                 console.log("Error: "+JSON.stringify(reason));
                 $(".loading-page").hide();

             });
       }
       else
       {
            document.getElementById('sub').innerHTML=suburb;
            getdata(suburb,doc_id);
       }
}

function getdata(suburb,doc_id)
{
    $.ajax({
         url: BASE_URL,
         type: "POST",
         dataType: "json",
         data: {suburb:suburb,doctor_id:doc_id,action: 'return_patient_appointment'},
         crossDomain: true
     }).done(function (data) {
     $(".loading-page").hide();
         if(data.response.id<=0)
         {
             document.getElementById('count').innerHTML= 0;
         }
         else
         {
             var str = $('.border-gray1').html();
             var res_data = JSON.stringify(data);
             var jsonData = JSON.parse(res_data);
             document.getElementById('count').innerHTML= jsonData.response.count;
             for (var i = 0; i < jsonData.response.data.length; i++) {
                 var counter = jsonData.response.data[i];
                 str += '<div class="border-gray" id="'+counter.id+'" onclick=appointmentid(this.id)>';
                 str += '<div class="col-lg-12 col-md-12 col-sm-12">';
                 str+='<div class="projects">';
                 str+='<ul><li><h2>Patient Name</h2><span class="sally">'+counter.fname+'</span></li>';
                 str+='<li><h2>Suburb of the patient</h2> <span>'+counter.street_name+'</span></li>';
                 str+='<li><h2>Dates</h2> <span>'+counter.appointmentdate+'</span></li>';
                 str+='<li><h2>Notes from Patient</h2> <span>'+counter.quick_desc+'</span></li>';
                 str+='<li><h2>Rating</h2> <span>4/5</span></li>';
                 str+='<li><h2>Submit Proposals</h2><span>38</span></li>';
                 str+='<li><h2>Appointment Creation Date</h2><span>'+counter.datetime+'</span></li>';
                 str+= '</ul></div></div></div>';
             }
             $('.border-gray1').html(str);
         }

     }).fail(function (reason) {
         console.log("Error: "+JSON.stringify(reason));
         $(".loading-page").hide();

     });
 }
function return_fav_list()
{
    var str="";
    $('.border-gray1').html(str);
    window.localStorage.removeItem("suburb");
       init();
}
function appointmentid(id)
{
   //document.getElementById(id).style.backgroundColor='#000000';
   $('.border-gray1').find('div').each(function(){
       var innerDivId = $(this).attr('id');
        if(!isNaN(innerDivId))
        {
           if(innerDivId==id)
           {
                 document.getElementById(id).style.backgroundColor='#cecece';
                 window.localStorage.setItem("appoint_id1", id);
           }
           else
           {
                document.getElementById(innerDivId).style.backgroundColor='#ffffff';
           }
        }
   });
}
function submit_proposal()
{
    var appoint_id=localStorage.getItem("appoint_id1");
    if(appoint_id===null)
    {
        alert("Please select detail first.");
        return false;
    }
    window.location="submit-proposal.html";
}

 function dashboard()
{
    window.location="doctor_dashboard.html";
}
function getcountry()
{
    window.location="open_appointment_location.html";

}