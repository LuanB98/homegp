function init() {

    var user_id=localStorage.getItem("user_id1");
    if(user_id)
    {
        window.location="doctor_dashboard.html";
    }
    window.localStorage.removeItem("appoint_id");
}

function init_dash() {

    window.localStorage.removeItem("appoint_id");
}
function getsuburbs()
{
    var res="";
    var str = "Alexander Heights,Alfred Cove,Applecross,Ardross,Armadale,Ascot,Ashfield,Attadale,Atwell,Balcatta,Balga,Ballajura,Bassendean,Bateman,Bayswater,Beaconsfield,Beckenham,Bedford,Bedfordale,Beechboro,Beechina,Beeliar,Beldon,Bellevue,Belmont,Bentley,Bertram,Bibra Lake,Bickley,Bicton,Booragoon,Boya,Brentwood,Brigadoon,Brookdale,Bull Creek,Bullsbrook,Burswood,Butler,Byford,Calista,Canning Vale,Cannington,Cardup,Carine,Carlisle,Carmel,Carramar,Casaurina,Caversham,Champion Lakes,Churchlands,City Beach,Claremont,Clarkson,Cloverdale,Como,Connolly,Coogee,Coolbellup,Coolbinia,Cooloongup,Cottesloe,Craigie,Crawley,Currambine,Daglish,Dalkeith,Darch,Darling Downs,Darlington,Dianella,Doubleview,Duncraig,East Cannington,East Fremantle,East Perth,East Victoria Park,Eden Hill,Edgewater,Ellenbrook,Embleton,Ferndale,Floreat,Forrestdale,Forrestfield,Fremantle,Girrawheen,Glen Forrest,Glendalough,Gooseberry Hill,Gosnells,Greenmount,Greenwood,Guildford,Gwelup,Hamersley,Hamilton Hill,Hazelmere,Heathridge,Helena Valley,Henley Brook,High Wycombe,Highgate,Hillarys,Hillman,Hilton,Hocking,Hovea,Huntingdale,Iluka,Inglewood,Innaloo,Jandakot,Jolimont,Joondalup,Joondanna,Kalamunda,Kallaroo,Karawara,Kardinya,Karrinyup,Kelmscott,Kensington,Kenwick,Kewdale,Kiara,Kingsley,Kinross,Koondoola,Koongamia,Landsdale,Langford,Lathlain,Leda,Leederville,Leeming,Lesmurdie,Lockridge,Lynwood,Maddington,Madeley,Mahogany Creek,Maida Vale,Manning,Marangaroo,Mariginup,Marmion,Martin,Maylands,Medina,Melville,Menora,Merriwa,Middle Swan,Midland,Midvale,Millendon,Mindarie,Mirrabooka,Morley,Mosman Park,Mount Claremont,Mount Hawthorn,Mount Lawley,Mount Nasura,Mount Pleasant,Mt Helena,Mt Richon,Mullaloo,Mundaring,Mundijong,Munster,Murdoch,Myaree,Nedlands,Neerabup,Nollamara,Noranda,North Beach,North Fremantle,North Lake,North Perth,Northbridge,Oakford,Ocean Reef,Oldsbury,Orelia,Osborne Park,Padbury,Palmyra,Parkwood,Parmelia,Pearsall,Peppermint Grove,Perth,Pickering Brook,Piesse Brook,Port Kennedy,Queens Park,Quinns Rocks,Redcliffe,Ridgewood,Riverton,Rivervale,Rockingham,Roleystone,Rossmoyne,Safety Bay,Salter Point,Samson,Scarborough,Secret Harbour,Seville Grove,Shelley,Shenton Park,Shoalwater,Sinagra,Singleton,Sorrento,South Fremantle,South Guildford,South Lake,South Perth,Southern River,Spearwood,St James,Stirling,Straffon,Subiaco,Success,Swan View,Swanbourne,Tapping,The Vines,Thornlie,Trigg,Tuart Hill,Two Rocks,Upper Swan,Victoria Park,Viveash,Waikiki,Walliston,Wandi,Wanneroo,Warnbro,Warwick,Waterford,Watermans Bay,Wattle Grove,Wattleup,Wellard,Wembley,Wembley Downs,West Leederville,West Perth,West Swan,Westfield,Westminster,Wexcombe,White Gum Valley,Willagee,Willetton,Wilson,Winthrop,Woodbridge,Woodlands,Woodvale,Yanchep,Yangebup,Yokine";
    var arr = str.split(",");
    res+='<select onchange="getallsub()" id="language" multiple>';
    for(i=0;i<arr.length;i++)
    {
         res+='<option style="font-size:50%;border-bottom:1px solid #cecece;padding:5px 0 0 0">'+arr[i]+'</option>';
    }
    res+='</select>';
    document.getElementById('suburbs').innerHTML=res;
}
function btn_click()
{
    getsuburbs();
}
function signup()
{
    window.location='doctor_signup.html';
}
function getallsub()
{
    document.getElementById('suburbs_data').value=$('#language').val();

}
function enable_postcode()
{
    var state=$('#state').val();
    if(state!='Select State')
    {
        document.getElementById("pincode").disabled = false;
        $('#pincode').focus();
    }
    else
    {
         document.getElementById("pincode").disabled = true;
         document.getElementById('pincode').value='';

    }
}

function doctor_signup()
{
    var values_of_suburbs=String($('#language').val());
    var res=values_of_suburbs.split(",");
    var fname=$('#fname').val();
    var lname=$('#lname').val();
    var email=$('#email').val();
    var password=$('#password').val();
    var mob=$('#mob').val();
    var ahpra=$('#ahpra').val();
    var qualification=$('#qualification').val();
    var doctor_since=$('#doctor_since').val();
    var stno=$('#stno').val();
    var stname=$('#stname').val();
    var suburb=$('#suburb').val();
    var state=$('#state').val();
    var pincode=$('#pincode').val();
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if ($("#4").attr("src") == "")
    {
        alert("Select profile pic first.");
        return false;
    }else if (fname == "") {
        alert("Please Enter First Name.");
        $('#fname').focus();
        return false;
     }else if (lname == "") {
        alert("Please Enter Last Name.");
        $('#lname').focus();
        return false;
     }else if (email == "") {
        alert("Please Enter Email-ID.");
        $('#email').focus();
        return false;
     }else if (!reg.test(email)){
        alert("Incorrect Email-ID format");
        $('#email').focus();
        return false;
     }else if (password == "") {
        alert("Please Enter Password.");
        $('#password').focus();
        return false;
     }else if (password.length <6 || password.length >12) {
        alert("Password must be between 6 to 12 digit.");
        $('#password').focus();
        return false;
     }else if (mob == "") {
        alert("Please Enter Mobile Number.");
        $('#mob').focus();
        return false;
     }else if (isNaN(mob)) {
          alert("Phone number must be a number.");
          $('#mob').focus();
          return false;
     }else if (mob.length!=10) {
          alert("Please Enter 10 digit phone no.");
          $('#mob').focus();
          return false;
     }else if (ahpra == "") {
        alert("Please Enter AHPRA no.");
        $('#ahpra').focus();
        return false;
     }else if (qualification == "") {
          alert("Please Enter qualification.");
          $('#qualification').focus();
          return false;
     }
     else if (doctor_since == "") {
        alert("Please Enter Date of first registration.");
        $('#doctor_since').focus();
        return false;
     }else if (res.length > 4) {
              alert("You can select only 4 suburbs");
              return false;
     }else if (res[0]==='null') {
              alert("Please select favourite suburb");
              return false;
     }else if (stno == "") {
        alert("Please Enter Street No.");
        $('#stno').focus();
        return false;
     }
     else if (stname == "") {
        alert("Please Enter Street Name.");
        $('#stname').focus();
        return false;
     }else if (suburb == "Select Suburb") {
        alert("Please Select Suburb.");
        $('#suburb').focus();
        return false;
     }else if (state == "Select State") {
        alert("Please Select State.");
        $('#state').focus();
        return false;
     }else if (pincode == "") {
        alert("Please Enter postcode.");
        $('#pincode').focus();
        return false;
     }else if (isNaN(pincode)) {
        alert("Postcode must be a number.");
        $('#pincode').focus();
        return false;
     }else if (pincode.length!=4) {
        alert("Please Enter 4 digit postcode.");
        $('#pincode').focus();
        return false;
     }
    else
    {
        // Upload profile pic
        var imageURI=localStorage.getItem("img4");
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.mimeType = "image/jpg";

        var params = new Object();
        params.imageURI = imageURI;
        params.imagename = (new Date).getTime()+".jpg";
        options.params = params;
        options.chunkedMode = false;
        var ft = new FileTransfer();
        var url = "http://35.162.172.171/doctorapp/upload.php";
        ft.upload(imageURI, url, win, fail, options, true);
        window.localStorage.setItem("img_name",params.imagename);
        function win(r) {
            window.localStorage.setItem("check",'done');
         }
        function fail(error) {

             window.localStorage.setItem("check",'');
        }
        function onFail(message) {
            //alert('Failed because: ' + message);
        }
        //Upload pic done
        if(localStorage.getItem("check")=="")
        {
            alert("There was an error uploading image");
            return false;
        }

        $(".loading-page").show();
        var profile_pic=localStorage.getItem("img_name");
         $.ajax({
            url: BASE_URL,
            type: "GET",
            dataType: "json",
            data:  {action:'register',type:'doctor',values_of_suburbs:values_of_suburbs,profile_pic:profile_pic,email:email,pass:password,ahpra_no:ahpra,fname:fname,
                            doctor_since:doctor_since,qualification:qualification,lname:lname,phone_no:mob,street_no:stno,street_name:stname,
                            suburb:suburb,state:state,pincode:pincode},
            crossDomain: true
        }).done(function (data) {
        $(".loading-page").hide();
            if(data.response.id>0)
            {
                   window.location="index.html";
            }
            else
            {
                alert(data.response.message);
            }

        }).fail(function (reason) {
        $(".loading-page").hide();
            console.log("Error: "+JSON.stringify(reason));

        });
        return false;
    }


}

function gettokens()
{
    var dev_token=localStorage.getItem("dev_token");
    if(dev_token==null || dev_token=='')
    {
        gettokens();
    }
    return true;


}

function login()
{

     var un=$('#username').val();
     var up=$('#password').val();
     var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
     if (un == "") {
        alert("Please Enter Username.");
        return false;
     }
     else if (up == "") {
        alert("Please Enter Password.");
        return false;
     }
     else if (!reg.test(un)){
        alert("Incorrect Email-ID format");
        return false;
     }
     else
     {
        app.addToCal();
        gettokens();
        var dev_token=localStorage.getItem("dev_token");
        $(".loading-page").show();
        $.ajax({
            url: BASE_URL,
            type: "POST",
            dataType: "json",
            data: {email: un, password: up,dev_token:dev_token,action:'login',type:'doctor'},
            crossDomain: true
        }).done(function (data) {
            $(".loading-page").hide();
            if(data.response.id<=0)
                alert(data.response.message);
            else
            {
                var id=data.response.id;
                window.localStorage.setItem("user_id1", id);
                window.location="doctor_dashboard.html";
             }


        }).fail(function (reason) {
        $(".loading-page").hide();
            console.log("Error: "+JSON.stringify(reason));

        });
     }

}
//Doctor redirected pages
function my_detail()
{
    window.location="doctor-profile.html";
}

function find_open_appointments()
{
    window.location="open_appointment.html";
}

function my_appointmnet_schedule()
{
    var doctor_id=localStorage.getItem("user_id1");
    $(".loading-page").show();
    $.ajax({
            url: BASE_URL,
            type: "POST",
            dataType: "json",
            data: {doc_id:doctor_id,action: 'doctorschedule'},
            crossDomain: true
        }).done(function (data) {
         $(".loading-page").hide();
            if(data.response.id<=0)
            {
                alert(data.response.message);
            }
            else
            {
                window.location="my-schedule.html";
            }

        }).fail(function (reason) {
         $(".loading-page").show();
            console.log("Error: "+JSON.stringify(reason));

        });

}

function closee()
{
    window.localStorage.removeItem("user_id1");
    window.location="index.html";
}

function getPhoto(source) {

     navigator.camera.getPicture(uploadPhoto4, onFail, { quality: 50,
             destinationType: navigator.camera.DestinationType.FILE_URI,
             sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
             encodingType: Camera.EncodingType.PNG
         });

}

function uploadPhoto4(imageURI) {
    var largeImage = document.getElementById('4');
    largeImage.style.display = 'block';
    largeImage.src = imageURI;
    window.localStorage.setItem("img4",imageURI);

}

function onFail(message) {
    //alert('Failed because: ' + message);
}
