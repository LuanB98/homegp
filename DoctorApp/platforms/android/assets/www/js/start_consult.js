function init() {
        window.localStorage.removeItem("edit_appoint_id1");
        window.localStorage.removeItem("appointid1");
        var doctor_id=localStorage.getItem("user_id1");
        var consult_data=localStorage.getItem("consult_data");
        $(".loading-page").show();
        $.ajax({
                url: BASE_URL,
                type: "POST",
                dataType: "json",
                data: {doc_id:doctor_id,action: 'doctorschedule'},
                crossDomain: true
            }).done(function (data) {
             $(".loading-page").hide();
                if(data.response.id<=0)
                {
                    alert(data.response.message);
                }
                else
                {
                    var str = $('.projects1').html();
                    var res_data = JSON.stringify(data);
                    var jsonData = JSON.parse(res_data);
                    for (var i = 0; i < jsonData.response.length; i++) {
                        var counter = jsonData.response[i];
                        window.localStorage.setItem("patient_id",counter.pid);
                        window.localStorage.setItem("patient_name",counter.fname);
                       // alert(appoint_id);
                        str += '<div class="border-gray" id="'+counter.id+'" onclick=appointmentid(this.id)>';
                        str += '<div class="col-lg-12 col-md-12 col-sm-12">';
                        str+='<div class="projects">';
                        str+='<ul><li><h2>Patient Name</h2><span class="sally" onclick=pateint_detail('+counter.id+')>'+counter.fname+'</span></li>';
                        str+='<li><h2>Suburb of the patient</h2> <span>'+counter.street_no+","+counter.street_name+","+counter.suburb+'</span></li>';
                        str+='<li><h2>Agreed Date</h2> <span>'+counter.appointmentdate+'</span></li>';
                        str+='<li><h2>Notes from Patient</h2> <span>'+counter.quick_desc+'</span></li>';
                        str+='<li><h2>My Notes</h2> <span>'+counter.note_on_patient+'</span></li>';
                        str+='<li><h2>Phone Number</h2> <span>'+counter.phone_no+'</span></li>';
                        str+= '</ul></div></div></div>';
                    }

                        $('.projects1').html(str);
                }

            }).fail(function (reason) {
             $(".loading-page").show();
                console.log("Error: "+JSON.stringify(reason));

            });
}

function insert_consult() {
      var patient_id=localStorage.getItem("patient_id");
      var patient_name=localStorage.getItem("patient_name");
      var item_no= $('#item_no').text();
      var item_desc= $('#post_desc').text();
      var Benefit_amt= $('#Benefit_amt').text();
      var patient_note= $('#patient_note').val();
      $(".loading-page").show();
        $.ajax({
            url: BASE_URL,
            type: "POST",
            dataType: "json",
            data: {action: 'insert_colsultation',patient_id:patient_id,patient_name:patient_name,item_name:'',item_desc:item_desc,item_number:item_no,benift:Benefit_amt,notes:patient_note},
            crossDomain: true
        }).done(function (data) {
        $(".loading-page").hide();
             if(data.response.id==1)
             {
                alert(data.response.message);
             }
             else
             {
                alert(data.response.message);
             }

        }).fail(function (reason) {
        $(".loading-page").hide();
            console.log("Error: "+JSON.stringify(reason));

        });


}
