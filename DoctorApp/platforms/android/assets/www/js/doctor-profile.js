function init() {
    // the next line makes it impossible to see Contacts on the HTC Evo since it
    // doesn't have a scroll button
    // document.addEventListener("touchmove", preventBehavior, false);
    //document.addEventListener("deviceready", deviceInfo, true);

    var user_id=localStorage.getItem("user_id1");
    //ret_fav_list(user_id);

    $(".loading-page").show();
        $.ajax({
            url: BASE_URL,
            type: "POST",
            dataType: "json",
            data: {id: user_id,action:'return_doctor_profile'},
            crossDomain: true
        }).done(function (data) {
            $(".loading-page").hide();
            if(data.response.id<=0)
                alert(data.response.message);
            else
            {
                 document.getElementById("doctor_since").innerHTML = data.response.doctor_since;
                 document.getElementById("doc_img").src = "http://legalknights.in/oldapi/proofs/"+data.response.profile_pic;
                 document.getElementById("qualification").innerHTML = data.response.qualification;
                 document.getElementById("about_me").innerHTML = data.response.aboutme;
                 document.getElementById("identity_location").value = data.response.identity_location;
                 document.getElementById("dname").innerHTML = data.response.fname+" "+data.response.lname;
                 document.getElementById("suburbs_data").value = data.response.fav_suburbs;
                 window.localStorage.setItem("suburb_list", data.response.fav_suburbs);
             }

        }).fail(function (reason) {
            console.log("Error: "+JSON.stringify(reason));
            $(".loading-page").hide();

        });
        getsuburbs();
    }

    function getsuburbs()
    {
       // var arr_fav_data = new Array();
        var fav_data=localStorage.getItem("suburb_list");
        var arr_fav_data = fav_data.split(",");
        var res="";
        var str = "Alexander Heights,Alfred Cove,Applecross,Ardross,Armadale,Ascot,Ashfield,Attadale,Atwell,Balcatta,Balga,Ballajura,Bassendean,Bateman,Bayswater,Beaconsfield,Beckenham,Bedford,Bedfordale,Beechboro,Beechina,Beeliar,Beldon,Bellevue,Belmont,Bentley,Bertram,Bibra Lake,Bickley,Bicton,Booragoon,Boya,Brentwood,Brigadoon,Brookdale,Bull Creek,Bullsbrook,Burswood,Butler,Byford,Calista,Canning Vale,Cannington,Cardup,Carine,Carlisle,Carmel,Carramar,Casaurina,Caversham,Champion Lakes,Churchlands,City Beach,Claremont,Clarkson,Cloverdale,Como,Connolly,Coogee,Coolbellup,Coolbinia,Cooloongup,Cottesloe,Craigie,Crawley,Currambine,Daglish,Dalkeith,Darch,Darling Downs,Darlington,Dianella,Doubleview,Duncraig,East Cannington,East Fremantle,East Perth,East Victoria Park,Eden Hill,Edgewater,Ellenbrook,Embleton,Ferndale,Floreat,Forrestdale,Forrestfield,Fremantle,Girrawheen,Glen Forrest,Glendalough,Gooseberry Hill,Gosnells,Greenmount,Greenwood,Guildford,Gwelup,Hamersley,Hamilton Hill,Hazelmere,Heathridge,Helena Valley,Henley Brook,High Wycombe,Highgate,Hillarys,Hillman,Hilton,Hocking,Hovea,Huntingdale,Iluka,Inglewood,Innaloo,Jandakot,Jolimont,Joondalup,Joondanna,Kalamunda,Kallaroo,Karawara,Kardinya,Karrinyup,Kelmscott,Kensington,Kenwick,Kewdale,Kiara,Kingsley,Kinross,Koondoola,Koongamia,Landsdale,Langford,Lathlain,Leda,Leederville,Leeming,Lesmurdie,Lockridge,Lynwood,Maddington,Madeley,Mahogany Creek,Maida Vale,Manning,Marangaroo,Mariginup,Marmion,Martin,Maylands,Medina,Melville,Menora,Merriwa,Middle Swan,Midland,Midvale,Millendon,Mindarie,Mirrabooka,Morley,Mosman Park,Mount Claremont,Mount Hawthorn,Mount Lawley,Mount Nasura,Mount Pleasant,Mt Helena,Mt Richon,Mullaloo,Mundaring,Mundijong,Munster,Murdoch,Myaree,Nedlands,Neerabup,Nollamara,Noranda,North Beach,North Fremantle,North Lake,North Perth,Northbridge,Oakford,Ocean Reef,Oldsbury,Orelia,Osborne Park,Padbury,Palmyra,Parkwood,Parmelia,Pearsall,Peppermint Grove,Perth,Pickering Brook,Piesse Brook,Port Kennedy,Queens Park,Quinns Rocks,Redcliffe,Ridgewood,Riverton,Rivervale,Rockingham,Roleystone,Rossmoyne,Safety Bay,Salter Point,Samson,Scarborough,Secret Harbour,Seville Grove,Shelley,Shenton Park,Shoalwater,Sinagra,Singleton,Sorrento,South Fremantle,South Guildford,South Lake,South Perth,Southern River,Spearwood,St James,Stirling,Straffon,Subiaco,Success,Swan View,Swanbourne,Tapping,The Vines,Thornlie,Trigg,Tuart Hill,Two Rocks,Upper Swan,Victoria Park,Viveash,Waikiki,Walliston,Wandi,Wanneroo,Warnbro,Warwick,Waterford,Watermans Bay,Wattle Grove,Wattleup,Wellard,Wembley,Wembley Downs,West Leederville,West Perth,West Swan,Westfield,Westminster,Wexcombe,White Gum Valley,Willagee,Willetton,Wilson,Winthrop,Woodbridge,Woodlands,Woodvale,Yanchep,Yangebup,Yokine";
        var arr = str.split(",");
        res+='<select onchange="lang1()" id="language" multiple>';
        for(i=0;i<arr.length;i++)
        {
             var flag=0;
             for(j=0;j<arr_fav_data.length;j++)
             {
                  if(arr[i]==arr_fav_data[j])
                  {
                      res+='<option style="font-size:50%;border-bottom:1px solid #cecece;padding:5px 0 0 0" selected>'+arr[i]+'</option>';
                       flag=1;
                  }
             }
             if(flag==0)
                res+='<option style="font-size:50%;border-bottom:1px solid #cecece;padding:5px 0 0 0">'+arr[i]+'</option>';
        }
        res+='</select>';
        document.getElementById('suburbs').innerHTML=res;
    }

    function lang1()
    {
        document.getElementById("suburbs_data").value=$('#language').val();
    }

    function mydata()
    {
        var values_of_suburbs=String($('#language').val());
        var res=values_of_suburbs.split(",");


        var user_id=localStorage.getItem("user_id1");
        var about=$("#about_me").val();
        var location=$("#identity_location").val();
        if(about=="")
        {
             alert("Please enter about detail");
             $('#about_me').focus();
             return false;
        }
        else if(location=="")
        {
             alert("Please enter location detail");
              $('#identity_location').focus();
              return false;

        }
        else if (res.length > 4) {
           alert("You can select only 4 suburbs");
           return false;
        }
        else if(res.length<1)
        {
            alert("Select suburb first");
            return false;
        }
        else
        {
            $(".loading-page").show();
            $.ajax({
                url: BASE_URL,
                type: "POST",
                dataType: "json",
                data: {id: user_id,values_of_suburbs:values_of_suburbs,about:about,location:location,action:'save_doctor_profile'},
                crossDomain: true
            }).done(function (data) {
                $(".loading-page").hide();
                if(data.response.id==-2)
                    alert(data.response.message);
                else
                {
                     alert(data.response.message);
                     init();
                }
                }).fail(function (reason) {
                console.log("Error: "+JSON.stringify(reason));
                $(".loading-page").hide();

            });
         }
    }

    function dashboard()
    {
        window.location="doctor_dashboard.html";
    }