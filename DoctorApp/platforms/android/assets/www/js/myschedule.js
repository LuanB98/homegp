function init() {
        window.localStorage.removeItem("edit_appoint_id1");
        window.localStorage.removeItem("appointid1");
        var doctor_id=localStorage.getItem("user_id1");
         $(".loading-page").show();
        $.ajax({
                url: BASE_URL,
                type: "POST",
                dataType: "json",
                data: {doc_id:doctor_id,action: 'doctorschedule'},
                crossDomain: true
            }).done(function (data) {
             $(".loading-page").hide();
                if(data.response.id<=0)
                {
                    alert(data.response.message);
                }
                else
                {
                    var str = $('.border-gray1').html();
                    var res_data = JSON.stringify(data);
                    var jsonData = JSON.parse(res_data);
                    for (var i = 0; i < jsonData.response.length; i++) {
                        var counter = jsonData.response[i];
                         window.localStorage.setItem("consult_data", counter);
                        str += '<div class="border-gray" id="'+counter.id+'" onclick=appointmentid(this.id)>';
                        str += '<div class="col-lg-12 col-md-12 col-sm-12">';
                        str+='<div class="projects">';
                        str+='<ul><li><h2>Patient Name</h2><span class="sally" onclick=pateint_detail('+counter.id+')>'+counter.fname+'</span></li>';
                        str+='<li><h2>Suburb of the patient</h2> <span>'+counter.street_no+","+counter.street_name+","+counter.suburb+'</span></li>';
                        str+='<li><h2>Agreed Date</h2> <span>'+counter.appointmentdate+'</span></li>';
                        str+='<li><h2>Notes from Patient</h2> <span>'+counter.quick_desc+'</span></li>';
                        str+='<li><h2>My Notes</h2> <span>'+counter.note_on_patient+'</span></li>';
                        str+='<li><h2>Phone Number</h2> <span>'+counter.phone_no+'</span></li>';
                        str+= '</ul></div></div></div>';
                    }

                        $('.border-gray1').html(str);
                }

            }).fail(function (reason) {
             $(".loading-page").show();
                console.log("Error: "+JSON.stringify(reason));

            });
}
function appointmentid(id)
{
   //document.getElementById(id).style.backgroundColor='#000000';
   $('.border-gray1').find('div').each(function(){
       var innerDivId = $(this).attr('id');
        if(!isNaN(innerDivId))
        {
           if(innerDivId==id)
           {
                 document.getElementById(id).style.backgroundColor='#cecece';
                 window.localStorage.setItem("edit_appoint_id1", id);
           }
           else
           {
                document.getElementById(innerDivId).style.backgroundColor='#ffffff';
           }
        }

   });

}
function dashboard()
{
    window.location="doctor_dashboard.html";
}
function edit_proposal()
{
    var loc=localStorage.getItem("edit_appoint_id1");
    if (loc !== null)
    {
        window.location="my_schedule_edit.html";
    }
    else
    {
        alert("Please select detail first.");
    }

}
function pateint_detail(appointid)
{
    window.localStorage.setItem("appointid1",appointid);
    window.location="patient_profile.html";
}

function strt_conslt()
{
 var  appoimnt_id=localStorage.getItem("edit_appoint_id1");;
   if(appoimnt_id !==null)
   {
      window.location="start_consultation.html";

   }
   else
   {
       alert("Please select detail first.");
   }


}