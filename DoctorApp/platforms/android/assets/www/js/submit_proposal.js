function init() {

    $(".loading-page").show();
    var appoint_id=localStorage.getItem("appoint_id1");
    if(appoint_id===null)
    {
        window.location="open_appointment.html";
    }
    document.getElementById("medivalue").disabled=true;
    $.ajax({
         url: BASE_URL,
         type: "POST",
         dataType: "json",
         data: {appoint_id:appoint_id,action: 'return_patient_detail_by_appoint_id'},
         crossDomain: true
     }).done(function (data) {
     $(".loading-page").hide();
         if(data.response.id<=0)
         {
             alert(data.response.message);
         }
         else
         {
              window.localStorage.setItem("patient_id1", data.response.pid);
              document.getElementById("fname").innerHTML = data.response.fname;
              document.getElementById("street_name").innerHTML = data.response.street_name;
              document.getElementById("appointmentdate").innerHTML = data.response.appointmentdate;
              document.getElementById("quick_desc").innerHTML = data.response.quick_desc;
              document.getElementById("datetime").innerHTML =data.response.datetime;
         }

     }).fail(function (reason) {
     $(".loading-page").hide();
         console.log("Error: "+JSON.stringify(reason));

     });
     fromdate();

}
function fromdate()
{
    var hrs1=[];
    var mins1=[];
    var timearr = new Array();
    timearr[1] = "01";
    timearr[2] = "02";
    timearr[3] = "03";
    timearr[4] = "04";
    timearr[5] = "05";
    timearr[6] = "05";
    timearr[7] = "07";
    timearr[8] = "08";
    timearr[9] = "09";
    timearr[10] = "10";
    timearr[11] = "11";
    timearr[11] = "12";

    var minarr = new Array();
    minarr[1] = "00";
    minarr[2] = "15";
    minarr[3] = "30";
    minarr[4] = "45";
    first_drop();
    function first_drop()
    {
            var days='<div class="full-new"><select id="day" disabled="disabled"><option>DD</option><option>Today</option><option>Tommorow</option></select></div>';
            var hrs='<div class="full-three"><select id="hrs_select" disabled="disabled" onchange=showYear(this.id)>';
            hrs=hrs+'<option>HH</option>';
            for(i=1;i<12;i++)
            {
                hrs=hrs+'<option>'+timearr[i]+'</option>'
                hrs1[i]=i;
            }
            hrs=hrs+"</select>";
            var mins='<select id="min_select" disabled="disabled" onchange=showYear(this.id)>';
            mins=mins+'<option>MM</option>';
            for(j=1;j<=4;j++)
            {
                mins=mins+'<option>'+minarr[j]+'</option>'
                mins1[j]=j;
            }
            mins=mins+"</select>";
            var pm='<select class="pm-time" id="pm-time" disabled="disabled"><option>AM</option><option>PM</option></select></div>';
            var fianlmins=days+""+hrs+""+mins+""+pm;
            document.getElementById("final").innerHTML=fianlmins;

     }

}
function confirm_proposal()
{

     var patient_note=document.getElementById('patient_note').value;
     var message_to_patient=document.getElementById('message_to_patient').value;
     var chk1=document.getElementById('chk1').checked;
     var chk2=document.getElementById('chk2').checked;
     var billing=document.getElementById('billing').checked;
     var medicare=document.getElementById('medicare').checked;
     var in_day=document.getElementById('in_day').value;
     var day=document.getElementById('day').value;
     var hrs_select=document.getElementById('hrs_select').value;
     var min_select=document.getElementById('min_select').value;
     var pm_time=document.getElementById('pm-time').value;
     var medivalue=document.getElementById('medivalue').value;

     if (patient_note == "") {
         alert("Please enter note for patient.");
         $('#patient_note').focus();
         return false;
     }
     if (chk1) {
         if(in_day=='Minutes')
         {
             alert("Please select Minutes.");
             return false;
         }
         var day="";
         var time=in_day;
     }
     if (chk2) {
          if(day=='Select Day')
          {
              alert("Please select day.");
              return false;
          }
          if(hrs_select=='HH')
          {
              alert("Please select hours.");
              return false;
          }
         if(min_select=='MM')
         {
             alert("Please select minutes.");
             return false;
         }
         var time=hrs_select+":"+min_select+" "+pm_time;
     }
     if(medicare)
     {

          if(medivalue=='')
          {
                alert("Please enter billing amount.");
                return false;
          }
          var billing_type='private';
          var medi_price=medivalue;
     }
     if(billing)
     {
        var billing_type='medicare';
        var medi_price='';
     }
     if(message_to_patient=='')
     {
         alert("Please enter messsage for patient");
          $('#message_to_patient').focus();
         return false;
     }
     var appoint_id=localStorage.getItem("appoint_id1");
     var pid=localStorage.getItem("patient_id1");
     var doctor_id=localStorage.getItem("user_id1");
     $(".loading-page").show();
     $.ajax({
            url: 'http://legalknights.in/oldapi/api.php?action=submit_proposal_by_doctor',
            type: "POST",
            dataType: "json",
            data: {action:'submit_proposal_by_doctor',pid:pid,appointment_id: appoint_id,doctor_id: doctor_id,note_on_patient: patient_note,message_to_patient:message_to_patient,in_day:in_day,
                billing_type:billing_type,billing:medi_price,proposed_time:time,proposed_day:day
            },
            crossDomain: true
        }).done(function (data) {
            $(".loading-page").hide();
            if(data.response.id<=0)
                alert(data.response.message);
            else
            {
                 alert(data.response.message);
                 window.location="open_appointment.html";
             }

        }).fail(function (reason) {
         $(".loading-page").hide();
            console.log("Error: "+JSON.stringify(reason));

        });
}

function valueChanged(id)
{
    if($('#chk1').is(":checked"))
    {
       var idd='chk1';
       fromdate();
       check(idd);
    }
    else
    {
        check("chk2");
    }
}
function check(data)
{
    if(data=='chk1')
    {
        document.getElementById("in_day").disabled=false;
        document.getElementById("day").disabled=true;
        document.getElementById("hrs_select").disabled=true;
        document.getElementById("min_select").disabled=true;
        document.getElementById("pm-time").disabled=true;

    }else if(data=='chk2')
    {
        document.getElementById("in_day").selectedIndex=0;
        document.getElementById("in_day").disabled=true;

        document.getElementById("day").disabled=false;
        document.getElementById("hrs_select").disabled=false;
        document.getElementById("min_select").disabled=false;
        document.getElementById("pm-time").disabled=false;
    }
}
function insert_price(id)
{
    var billing=document.getElementById('billing').checked;
    var medicare=document.getElementById('medicare').checked;
    if(billing)
    {
        document.getElementById("medivalue").disabled = true;
        document.getElementById("doc_price").innerHTML ='';
    }
    else
    {
        document.getElementById("medivalue").disabled = false;
    }
}

function dashboard()
{
    window.location="doctor_dashboard.html";
}
function getval(id)
{
    document.getElementById("doc_price").innerHTML =id;
}