<?php
include_once('config.php'); //$pdo

$action=$_REQUEST['action'];
switch($action)
{
	
	case 'register':
		register();
		break;
	case 'return_favourite_list':
		return_favourite_list();
		break;
	case 'update_patient_detail':
		update_patient_detail();
		break;
	case 'check_email':
		check_email();
		break;
	case 'login':
		login();
		break;
	case 'admin_login':
		admin_login();
		break;
	case 'return_doctor_profile':
		return_doctor_profile();
		break;
	case 'save_doctor_profile':
		save_doctor_profile();
		break;
	case 'return_patient_detail':
		return_patient_detail();
		break;
	case 'save_patient_appointment':
		save_patient_appointment();
		break;
	case 'return_patient_appointment':
		return_patient_appointment();
		break;
	case 'return_patient_detail_by_appoint_id':
		return_patient_detail_by_appoint_id();
		break;
	case 'submit_proposal_by_doctor':
		submit_proposal_by_doctor();
		break;
	case 'doctorschedule':
		doctorschedule();
		break;
	case 'edit_doctor_schedule':
		edit_doctor_schedule();
		break;
	case 'return_edit_myschedule':
		return_edit_myschedule();
		break;
	case 'cancel_doctor_schedule':
		cancel_doctor_schedule();
		break;
	case 'return_patient_profile_for_doc':
		return_patient_profile_for_doc();
		break;
	case 'return_doctor_appointments':
		return_doctor_appointments();
		break;
	case 'approve_doctor_by_patient':
		approve_doctor_by_patient();
		break;
	case 'current_status':
		current_status();
		break;
	case 'chat':
		chat();
		break;
	case 'return_chat':
		return_chat();
		break;
	case 'return_date_of_appointment':
		return_date_of_appointment();
		break;
	case 'cancel_appointment':
		cancel_appointment();
		break;
	case 'return_edit_current_open_appointment':
		return_edit_current_open_appointment();
		break;
	case 'update_current_open_appointment':
		update_current_open_appointment();
		break;
	case 'truncate':
		truncate();
		break;
}

function register()
{
	// http://mobile.csgroupdevhub.com/doctorapp/api.php?action=register&type=pati&email=abc@gmail.com&password=123&doctor_since=2016&ahpra_no=abc&fname=test&lname=yes&phone_no=123&street_no=1&street_name=asd&suburb=jal&state=pb&pincode=10&qualification=mca
	global $pdo;
	extract($_REQUEST);
	//insert new user
	$sel_qry="select * from register where email=:email and type=:type";
	$stmt=$pdo->prepare($sel_qry);
	$sel_array=array(':email'=>$email,':type'=>$type);
	$stmt->execute($sel_array);
	$row=$stmt->fetch(PDO::FETCH_ASSOC);
	if(!empty($row))
	{
		$json['id']=-1;
		$json['message']="Email-ID already exist";
	}
	else
	{
		switch($state)
		{
			
			case 'NSW':
				$chk_code=NSW($pincode);
				break;
			case 'ACT':
				$chk_code=ACT($pincode);
				break;
			case 'NT':
				$chk_code=NT($pincode);
				break;
			case 'QLD':
				$chk_code=QLD($pincode);
				break;
			case 'SA':
				$chk_code=SA($pincode);
				break;
			case 'TAS':
				$chk_code=TAS($pincode);
				break;
			case 'VIC':
				$chk_code=VIC($pincode);
				break;
			case 'WA':
				$chk_code=WA($pincode);
				break;
		}
		if($chk_code)
		{
			if($type=='doctor')
			{
				$qry="insert into register(fav_suburbs,type,regtime,profile_pic,email,password,doctor_since,qualification,ahpra_no,fname,lname,phone_no,street_no,street_name,suburb,state,pincode) 
									values(:values_of_suburbs,:type,NOW(),:profile_pic,:email,:password,:doctor_since,:qualification,:ahpra_no,:fname,:lname,:phone_no,:street_no,:street_name,:suburb,:state,:pincode)";
				$stmt_qry=$pdo->prepare($qry);
				$pass=sha1($password);
				$arr=array(':type'=>$type,':values_of_suburbs'=>$values_of_suburbs,':profile_pic'=>$profile_pic,':email'=>$email,':password'=>$pass,':doctor_since'=>$doctor_since,':qualification'=>$qualification,':ahpra_no'=>$ahpra_no,':fname'=>$fname,':lname'=>$lname,':phone_no'=>$phone_no,
				':street_no'=>$street_no,':street_name'=>$street_name,':suburb'=>$suburb,':state'=>$state,':pincode'=>$pincode);
			}
			else
			{
				$qry="insert into register(type,regtime,email,password,fname,lname,phone_no,street_no,street_name,suburb,state,pincode,dob,card_no,card_type,gender) 
									values(:type,NOW(),:email,:password,:fname,:lname,:phone_no,:street_no,:street_name,:suburb,:state,:pincode,:dob,:card_no,:card_type,:gender)";
				$stmt_qry=$pdo->prepare($qry);
				$pass=sha1($password);
				$dob=$dayselect." ".$monthselect." ".$yearselect;
				$arr=array(':type'=>$type,':email'=>$email,':password'=>$pass,':fname'=>$fname,':lname'=>$lname,':phone_no'=>$mob,
				':street_no'=>$stno,':street_name'=>$stname,':suburb'=>$suburb,':state'=>$state,':pincode'=>$pincode,':dob'=>$dob,':card_no'=>$card_no,'card_type'=>$card_type,':gender'=>$gender);
			}
			$stmt_qry->execute($arr);
			if($stmt_qry->rowCount())
			{
				$json['id']=$pdo->lastInsertId();
				$json['type']=$type;
				$json['message']="Registered Successfully";
			}
			else
			{
				$json['id']=-2;
				$json['message']="Server Error";		
			}
		}
		else
		{
			$json['id']=-2;
			$json['message']="Invalid postcode for the state";
		}
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}
function update_patient_detail()
{
	// http://mobile.csgroupdevhub.com/doctorapp/api.php?action=update_patient_detail&
	global $pdo;
	extract($_REQUEST);
	switch($state)
	{
		
		case 'NSW':
			$chk_code=NSW($pincode);
			break;
		case 'ACT':
			$chk_code=ACT($pincode);
			break;
		case 'NT':
			$chk_code=NT($pincode);
			break;
		case 'QLD':
			$chk_code=QLD($pincode);
			break;
		case 'SA':
			$chk_code=SA($pincode);
			break;
		case 'TAS':
			$chk_code=TAS($pincode);
			break;
		case 'VIC':
			$chk_code=VIC($pincode);
			break;
		case 'WA':
			$chk_code=WA($pincode);
			break;
	}
	if($chk_code)
	{
		$qry="update register set fname=:fname,lname=:lname,phone_no=:phone_no,street_no=:street_no,street_name=:street_name,suburb=:suburb,state=:state,pincode=:pincode,dob=:dob,card_no=:card_no,card_type=:card_type,gender=:gender where id=:user_id"; 
		$stmt_qry=$pdo->prepare($qry);
		$pass=sha1($password);
		$dob=$dayselect." ".$monthselect." ".$yearselect;
		$arr=array(':user_id'=>$user_id,':fname'=>$fname,':lname'=>$lname,':phone_no'=>$mob,
			':street_no'=>$stno,':street_name'=>$stname,':suburb'=>$suburb,':state'=>$state,':pincode'=>$pincode,':dob'=>$dob,':card_no'=>$card_no,'card_type'=>$card_type,':gender'=>$gender);
		$stmt_qry->execute($arr);
		if($stmt_qry->rowCount())
		{
			$json['id']=1;
			$json['message']="Detail updated Successfully";
		}
		else
		{
			$json['id']=-1;
			$json['message']="Nothing to update";		
		}
	}
	else
	{
		$json['id']=-2;
		$json['message']="Invalid postcode for the state";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}
function save_patient_appointment()
{
	// http://mobile.csgroupdevhub.com/doctorapp/api.php?action=save_patient_appointment&id=pati&quick_desc=abc@gmail.com&skills=123&fromdate=2016&todate=abc&street=test&st_name=yes&suburb=jal&state=pb&pincode=10&final_date=ASD
	global $pdo;
	extract($_REQUEST);
	//insert new user
	//street,st_name,suburb,pincode,state,quick_desc,skills,fromdate,todate,final_date
	switch($state)
	{
		
		case 'NSW':
			$chk_code=NSW($pincode);
			break;
		case 'ACT':
			$chk_code=ACT($pincode);
			break;
		case 'NT':
			$chk_code=NT($pincode);
			break;
		case 'QLD':
			$chk_code=QLD($pincode);
			break;
		case 'SA':
			$chk_code=SA($pincode);
			break;
		case 'TAS':
			$chk_code=TAS($pincode);
			break;
		case 'VIC':
			$chk_code=VIC($pincode);
			break;
		case 'WA':
			$chk_code=WA($pincode);
			break;
	}
	if($chk_code)
	{
		$sel_qry_sts="select * from patient_request where payment_status='no' and pid=:id";
		$stmt_sts=$pdo->prepare($sel_qry_sts);
		$id_arr=array(':id'=>$id);
		$stmt_sts->execute($id_arr);
		if(!$stmt_sts->rowCount())
		{		
			if($final_date=='ASAP')
			{
				 
				 $day=date('D',$final_date_timestamp);
				 switch($day)
				 {
					 case 'Fri':
							$date_timestamp='Friday '.date('d/m/Y', $final_date_timestamp);
							break;
					 case 'Mon':
							$date_timestamp='Monday '.date('d/m/Y', $final_date_timestamp);
							break;
					 case 'Tue':
							$date_timestamp='Tuesday '.date('d/m/Y', $final_date_timestamp);
							break;
					 case 'Wed':
							$date_timestamp='Wednesday '.date('d/m/Y', $final_date_timestamp);
							break;
					 case 'Thu':
							$date_timestamp='Thursday '.date('d/m/Y', $final_date_timestamp);
							break;
					 case 'Sat':
							$date_timestamp='Saturday '.date('d/m/Y', $final_date_timestamp);
							break;
					 case 'Sun':
							$date_timestamp='Sunday '.date('d/m/Y', $final_date_timestamp);
							break;
				 }	
				 $todate=date('h:i A', $final_date_timestamp+21600);
				 //$final_date=date('d/M/Y', $fromdate);
				 $fromdate=date('h:i A', $final_date_timestamp);
			}	
			else
			{
				$date_timestamp=$final_date;
				//$date_timestamp='Friday 26/02/2016';
				 $final_date='Next7';
			}
			$checkdate="select * from patient_request where final_date_timestamp=:final_date_timestamp and pid=:pid";
			$stmt_checkdate=$pdo->prepare($checkdate);
			$arr_checkdate=array(':final_date_timestamp'=>$date_timestamp,':pid'=>$id);
			$stmt_checkdate->execute($arr_checkdate);
			$row=$stmt_checkdate->fetchAll(PDO::FETCH_ASSOC);
			$check_date=1;
			foreach($row as $check)
			{
				if((strtotime($check['from_time'])<=strtotime($fromdate) && strtotime($check['to_time'])>=strtotime($fromdate)) || 	(strtotime($check['from_time'])<=strtotime($todate) && strtotime($check['to_time'])>=strtotime($todate)))
				{
					$check_date=0;
				}
			}
			if($check_date)
			{
				$qry="insert into patient_request(pid,quick_desc,skill_require,from_time,to_time,street_no,street_name,suburb,state,pincode,appointmentdate,datetime,final_date_timestamp,location_type) 
									values(:id,:quick_desc,:skills,:fromdate,:todate,:street,:street_name,:suburb,:state,:pincode,:final_date,NOW(),:final_date_timestamp,:location_type)";
				$stmt_qry=$pdo->prepare($qry);
				$arr=array(':id'=>$id,':quick_desc'=>$quick_desc,':skills'=>$skills,':fromdate'=>$fromdate,':todate'=>$todate,':street'=>$street,
					':street_name'=>$st_name,':suburb'=>$suburb,':state'=>$state,':location_type'=>$location_type,':pincode'=>$pincode,':final_date'=>$final_date,':final_date_timestamp'=>$date_timestamp);
				$stmt_qry->execute($arr);
				if($stmt_qry->rowCount())
				{
					//get user name
					$check_user="select fname from register where id=:id";
					$stmt_check_user=$pdo->prepare($check_user);
					$stmt_check_user->execute(array(':id'=>$id));
					$row_user=$stmt_check_user->fetch(PDO::FETCH_ASSOC);
					//get fav_suburbs
					$checkdate1="select id,dev_token,fav_suburbs from register where type='doctor'";
					$stmt_checkdate1=$pdo->prepare($checkdate1);
					$stmt_checkdate1->execute();
					$row1=$stmt_checkdate1->fetchAll(PDO::FETCH_ASSOC);
					foreach($row1 as $r)
					{
						$sub_list=array();
						$sub_list=explode(",",$r['fav_suburbs']);
						if(in_array($suburb,$sub_list))
						{
							if(!empty($r['dev_token']))
							{
								if(strlen($r['dev_token'])>100)
								{
									include_once('mgcm.php'); 
									$message = $row_user['fname'].' looking for a doctor';
									$registrationIdss = $r['dev_token'];
									send_android_notification($registrationIdss,$message);
								}
								else
								{
									//send_iphone_notification($registrationIdss,$message);
								}
							}
						}
					}
					$json['id']=$pdo->lastInsertId();
					$json['message']="Registered Successfully";
				}
				else
				{
					$json['id']=-2;
					$json['message']="Server Error";		
				}
			}
			else
			{
				$json['id']=-2;
				$json['message']="Please select another date";	
			}
		}
		else
		{
			$json['id']=-2;
			$json['message']="You already have booked an appointment";
		}
	}	
	else
	{
		$json['id']=-2;
		$json['message']="Invalid postcode";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}

function check_email()
{
	//~ http://mobile.csgroupdevhub.com/doctorapp/api.php?action=check_email&type=pati&email=abc@gmail.co
	global $pdo;
	extract($_REQUEST);
	//Check Email exist or not
	$sel_qry="select * from register where email=:email and type=:type";
	$stmt=$pdo->prepare($sel_qry);
	$sel_array=array(':email'=>$email,':type'=>$type);
	$stmt->execute($sel_array);
	$row=$stmt->fetch(PDO::FETCH_ASSOC);
	if(!empty($row))
	{
		$json['id']=-1;
		$json['message']="Email-ID already exist";
	}
	else
	{
		$json['id']=1;
		$json['message']="Email-ID doesnt esixt";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}

function login()
{
	//~ http://mobile.csgroupdevhub.com/trivia_app/ws/api.php?action=login&email=abc@gmail.com&password=abc&type=doctor
	global $pdo;
	extract($_REQUEST);
	//Check Email exist or not
	$sel_qry="select * from register where email=:email and type=:type";
	$stmt=$pdo->prepare($sel_qry);
	$sel_array=array(':email'=>$email,':type'=>$type);
	$stmt->execute($sel_array);
	$row=$stmt->fetch(PDO::FETCH_ASSOC);
	if(!empty($row))
	{
		$pass=sha1($password);
		//Check password match or not	
		if($row['password']==$pass)
		{
			if($type=='doctor')
			{
				if(empty($row['status']))
				{
					$json['id']=-3;
					$json['message']="Your account is not yet approved";
				}
				else
				{
					$upd_qry="update register set dev_token=:dev_token where id=:id";
					$stmt_qry=$pdo->prepare($upd_qry);
					$upd_arr=array(':dev_token'=>$dev_token,':id'=>$row['id']);
					$stmt_qry->execute($upd_arr);
					$json['id']=$row['id'];
					$json['type']='doctor';
					$json['message']="Login Successfull";
				}
			}
			else
			{
				$upd_qry="update register set dev_token=:dev_token where id=:id";
				$stmt_qry=$pdo->prepare($upd_qry);
				$upd_arr=array(':dev_token'=>$dev_token,':id'=>$row['id']);
				$stmt_qry->execute($upd_arr);
				$json['id']=$row['id'];
				$json['type']='patient';
				$json['message']="Login Successfull";
			}
		}
		else
		{
			$json['id']=-2;
			$json['message']="Incorrect Password";
		}
	}
	else
	{
		$json['id']=-1;
		$json['message']="Incorrect Email-ID";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}

function return_doctor_profile()
{
	//~ http://mobile.csgroupdevhub.com/doctorapp/api.php?action=return_doctor_profile&id=1
	global $pdo;
	extract($_REQUEST);
	//Check Email exist or not
	$sel_qry="select * from register where id=:id";
	$stmt=$pdo->prepare($sel_qry);
	$sel_array=array(':id'=>$id);
	$stmt->execute($sel_array);
	$json=$stmt->fetch(PDO::FETCH_ASSOC);
	if(empty($json))
	{
		$json['id']=-1;
		$json['message']="No detail found";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}

function return_favourite_list()
{
	//~ http://mobile.csgroupdevhub.com/doctorapp/api.php?action=return_favourite_list&doctor_id=1
	global $pdo;
	extract($_REQUEST);
	//Check Email exist or not
	$sel_qry="select fav_suburbs from register where id=:doctor_id";
	$stmt=$pdo->prepare($sel_qry);
	$sel_array=array(':doctor_id'=>$doctor_id);
	$stmt->execute($sel_array);
	$json=$stmt->fetch(PDO::FETCH_ASSOC);
	if(empty($json))
	{
		$json['id']=-1;
		$json['message']="No detail found";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}


function return_patient_detail()
{
	//~ http://mobile.csgroupdevhub.com/doctorapp/api.php?action=return_patient_detail&id=1
	global $pdo;
	extract($_REQUEST);
	//Check Email exist or not
	$sel_qry="select * from register where id=:id";
	$stmt=$pdo->prepare($sel_qry);
	$sel_array=array(':id'=>$id);
	$stmt->execute($sel_array);
	$json=$stmt->fetch(PDO::FETCH_ASSOC);
	if(empty($json))
	{
		$json['id']=-1;
		$json['message']="No Patient Detail";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}

function return_patient_appointment()
{
	//~ http://mobile.csgroupdevhub.com/doctorapp/api.php?action=return_patient_appointment&suburb=jna
	global $pdo;
	extract($_REQUEST);
	//Check Email exist or not
	$suburb_data=explode(",",$suburb);
	$json=array();
	$count=0;
	foreach($suburb_data as $suburb_list)
	{
		$suburb_res=trim($suburb_list);
		
		$sel_qry="select p.id,r.phone_no,r.fname,p.street_name,p.appointmentdate,p.quick_desc,p.datetime from patient_request p,register r where 
		r.id=p.pid  and p.id NOT IN (select appointment_id from appointment_list where doctor_id=$doctor_id and status='1') and p.suburb LIKE '$suburb_res' and p.status=''";
		$stmt=$pdo->prepare($sel_qry);
		$stmt->execute();
		$result=$stmt->fetchAll(PDO::FETCH_ASSOC);
		if($result)
		{

			foreach($result as $res)
			{
				$json['data'][]=array('id'=>$res['id'],'fname'=>$res['fname'],'street_name'=>$res['street_name'],'appointmentdate'=>$res['appointmentdate'],
				'quick_desc'=>$res['quick_desc'],'datetime'=>$res['datetime'],'phone_no'=>$res['phone_no']);
				$count++;
			}
			$json['count']=$count;
		}
		
	}
	if(empty($json))
	{
		$json['id']=-1;
		$json['message']="No Patient Record Found";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}

function return_patient_detail_by_appoint_id()
{
	//~ http://mobile.csgroupdevhub.com/doctorapp/api.php?action=return_patient_detail_by_appoint_id&appoint_id=1
	global $pdo;
	extract($_REQUEST);
	//Check Email exist or not
	$sel_qry="select p.id,p.pid,r.fname,p.street_name,p.appointmentdate,p.quick_desc,p.datetime from patient_request p,register r where r.id=p.pid and p.id=$appoint_id";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute();
	$json=$stmt->fetch(PDO::FETCH_ASSOC);
	if(empty($json))
	{
		$json['id']=-1;
		$json['message']="No Patient Record Found";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}

function save_doctor_profile()
{
	//~ http://mobile.csgroupdevhub.com/trivia_app/ws/api.php?action=save_doctor_profile&id=1&about=doc&location=paris
	
	global $pdo;
	extract($_REQUEST);
	$upd_qry="update register set fav_suburbs=:values_of_suburbs,aboutme=:about,identity_location=:location where id=:id";
	$stmt_qry=$pdo->prepare($upd_qry);
	$upd_arr=array(':about'=>$about,':location'=>$location,':id'=>$id,':values_of_suburbs'=>$values_of_suburbs);
	$stmt_qry->execute($upd_arr);
	$json['id']=1;
	$json['message']="Profile updated successfully";
	
	echo "{\"response\":" . json_encode($json) . "}";
}

function admin_login()
{
	//~ http://mobile.csgroupdevhub.com/trivia_app/ws/api.php?action=admin_login&email=abc@gmail.com&password=abc
	global $pdo;
	extract($_REQUEST);
	//Check Email exist or not
	$sel_qry="select * from admin where email=:email";
	$stmt=$pdo->prepare($sel_qry);
	$sel_array=array(':email'=>$email);
	$stmt->execute($sel_array);
	$row=$stmt->fetch(PDO::FETCH_ASSOC);
	if(!empty($row))
	{
		$pass=sha1($password);
		//Check password match or not	
		if($row['password']==$pass)
		{
			$json['id']=$row['id'];
			$json['type']=$row['type'];
			$json['message']="Login Successfully";
		}
		else
		{
			$json['id']=-2;
			$json['message']="Invalid Password";
		}
	}
	else
	{
		$json['id']=-1;
		$json['message']="Invalid Email-ID";
	}
	echo "{\"response\":" . json_encode($json) . "}";
	
}

function submit_proposal_by_doctor()
{
	//~ http://mobile.csgroupdevhub.com/trivia_app/ws/api.php?action=submit_proposal_by_doctor&id=1&about=doc&location=paris
	
	global $pdo;
	extract($_REQUEST);
	$ins_qry="insert into appointment_list(appointment_id,status,pid,doctor_id,note_on_patient,billing,billing_type,message_to_patient,proposed_time,proposed_day,appointment_confrim_date) values(:appointment_id,:status,:pid,:doctor_id,:note_on_patient,:billing,:billing_type,:message_to_patient,:proposed_time,:proposed_day,NOW())";
	$stmt_qry=$pdo->prepare($ins_qry);
	$ins_arr=array('appointment_id'=>$appointment_id,':status'=>1,':pid'=>$pid,'proposed_day'=>$proposed_day,'proposed_time'=>$proposed_time,'doctor_id'=>$doctor_id,'note_on_patient'=>$note_on_patient,'billing'=>$billing,'message_to_patient'=>$message_to_patient,'billing_type'=>$billing_type);
	$stmt_qry->execute($ins_arr);
	if($stmt_qry->rowCount())
	{
		//get doctor name for sending notification
		$sel_qry="select fname from register where id=:doctor_id";
		$stmt=$pdo->prepare($sel_qry);
		$sel_array=array(':doctor_id'=>$doctor_id);
		$stmt->execute($sel_array);
		$row=$stmt->fetch(PDO::FETCH_ASSOC);
		
		//get patient detail for sending notification
		$sel_qry1="select dev_token from register where id=:pid";
		$stmt1=$pdo->prepare($sel_qry1);
		$sel_array1=array(':pid'=>$pid);
		$stmt1->execute($sel_array1);
		$row1=$stmt1->fetch(PDO::FETCH_ASSOC);
		if(!empty($row1['dev_token']))
		{
			if(strlen($row1['dev_token'])>100)
			{
				include_once('mgcm.php'); 
				$message = $row['fname'].' waiting for approval';
				$registrationIdss = $row1['dev_token'];
				send_android_notification($registrationIdss,$message);
			}
			else
			{
				//send_iphone_notification($registrationIdss,$message);
			}
		}
		
		$json['id']=1;
		$json['message']="Appointment done";
	}
	else
	{
		$json['id']=-2;
		$json['message']="Appointment not done";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function doctorschedule()
{
	global $pdo;
	extract($_REQUEST);
	//$sel_qry="select id from appointment_list where doctor_id=$doc_id and status=1";
	$sel_qry="select p.id,p.pid,r.fname,r.phone_no,p.street_no,p.suburb,p.street_name,p.suburb,p.appointmentdate,p.quick_desc,p.datetime,note_on_patient from appointment_list a,patient_request p,register r where p.id=a.appointment_id and r.id=p.pid and a.id IN(select id from appointment_list where doctor_id=$doc_id and status=2)";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute();
	$json=$stmt->fetchAll(PDO::FETCH_ASSOC);
	if(empty($json))
	{
		$json['id']=-1;
		$json['message']="No Patient Record Found";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function return_edit_myschedule()
{
	global $pdo;
	extract($_REQUEST);
	$sel_qry="select a.id,r.fname,proposed_time,proposed_day,note_on_patient from appointment_list a,register r where a.pid=r.id and appointment_id=$appoint_id";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute();
	$json=$stmt->fetch(PDO::FETCH_ASSOC);
	if(!$stmt->rowCount())
	{
		$json['id']=-2;
		$json['message']="No record found";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function return_patient_profile_for_doc()
{
	global $pdo;
	extract($_REQUEST);
	$sel_qry="select p.id,p.quick_desc,p.skill_require,r.fname,r.lname,r.street_no,r.street_name,r.suburb,a.note_on_patient from appointment_list a,patient_request p,register r where p.pid=r.id and 
	 a.appointment_id=p.id and p.id=$appointid";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute();
	$json=$stmt->fetch(PDO::FETCH_ASSOC);
	if(!$stmt->rowCount())
	{
		$json['id']=-2;
		$json['message']="No record found";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function edit_doctor_schedule()
{
	global $pdo;
	extract($_REQUEST);
	$upd_qry="update appointment_list set note_on_patient=:note_on_patient,proposed_day=:proposed_day,proposed_time=:proposed_time where appointment_id=:appoint_id";
	$stmt_qry=$pdo->prepare($upd_qry);
	$upd_arr=array(':appoint_id'=>$appoint_id,':proposed_time'=>$final_day,':proposed_day'=>$day,':note_on_patient'=>$note_to_patient);
	$stmt_qry->execute($upd_arr);	
	if($stmt_qry->rowCount())
	{
		
		$json['id']=1;
		$json['message']="Details updated";
	}
	else
	{
		$json['id']=-2;
		$json['message']="Details not updated";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function cancel_doctor_schedule()
{
	global $pdo;
	extract($_REQUEST);
	$arr=array(':appoint_id'=>$appoint_id);
	$del_qry="delete from appointment_list where appointment_id=:appoint_id";
	$stmt_qry=$pdo->prepare($del_qry);	
	$stmt_qry->execute($arr);	
	if($stmt_qry->rowCount())
	{
		$upd_qry="update patient_request set status='' where id=:appoint_id";
		$stmt_qry1=$pdo->prepare($upd_qry);
		$stmt_qry1->execute($arr);	
		$json['id']=1;
		$json['message']="Proposal cancelled";
	}
	else
	{
		$json['id']=-2;
		$json['message']="Proposal not cancelled";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function return_doctor_appointments()
{
	global $pdo;
	extract($_REQUEST);
	$result=1;
	$arr=array(':user_id'=>$user_id,':appointment_id'=>$appointment_id);
	$check_qry="select status from appointment_list where  appointment_id=:appointment_id and pid=:user_id and status='2'";
	$stmt_check_qry=$pdo->prepare($check_qry);
	$stmt_check_qry->execute($arr);
	$data=$stmt_check_qry->fetchAll(PDO::FETCH_ASSOC);
	if($stmt_check_qry->rowCount())
	{
		$result=0;
	}
	if($result)
	{
		$sel_qry="select appointment_id,doctor_id,note_on_patient,proposed_time,proposed_day,billing_type,billing,fname,lname,profile_pic from appointment_list a,register r where  a.doctor_id=r.id and a.pid=:user_id and a.appointment_id=:appointment_id and a.status='1'";
		$stmt=$pdo->prepare($sel_qry);
		$stmt->execute($arr);
		$json=$stmt->fetchAll(PDO::FETCH_ASSOC);
		if(!$stmt->rowCount())
		{
			$json['appointment_id']=-2;
			$json['message']="No proposal found";
		}
	}
	else
	{
		$json['appointment_id']=-2;
		$json['message']="Doctor already booked for this appointment";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function approve_doctor_by_patient()
{
	
	global $pdo;
	extract($_REQUEST);
	$upd_arr=array(':id'=>$appointment_id);
	$upd_qry="update patient_request set status=1 where id=:id";
	$stmt_qry=$pdo->prepare($upd_qry);
	$stmt_qry->execute($upd_arr);	
	
	$upd_arr1=array(':id'=>$appointment_id,':doc_id'=>$doctor_id,'pid'=>$pid);
	$upd_qry1="update appointment_list set status=2 where appointment_id=:id and doctor_id=:doc_id and pid=:pid";
	$stmt_qry1=$pdo->prepare($upd_qry1);
	$stmt_qry1->execute($upd_arr1);	
	if($stmt_qry1->rowCount())
	{
		//get doctor name for sending notification
		$sel_qry="select fname from register where id=:pid";
		$stmt=$pdo->prepare($sel_qry);
		$sel_array=array(':pid'=>$pid);
		$stmt->execute($sel_array);
		$row=$stmt->fetch(PDO::FETCH_ASSOC);
		
		//get patient detail for sending notification
		$sel_qry1="select dev_token from register where id=:doctor_id";
		$stmt1=$pdo->prepare($sel_qry1);
		$sel_array1=array(':doctor_id'=>$doctor_id);
		$stmt1->execute($sel_array1);
		$row1=$stmt1->fetch(PDO::FETCH_ASSOC);
		
		if(!empty($row1['dev_token']))
		{
			if(strlen($row1['dev_token'])>100)
			{
				include_once('mgcm.php'); 
				$message = $row['fname'].' has accepted your request';
				$registrationIdss = $row1['dev_token'];
				send_android_notification($registrationIdss,$message);
			}
			else
			{
				//send_iphone_notification($registrationIdss,$message);
			}
		}
		
		$json['id']=1;
		$json['message']="Doctor booked";
	}
	else
	{
		$json['id']=-1;
		$json['message']="Doctor not booked";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}
function current_status()
{
	global $pdo;
	extract($_REQUEST);
	$arr=array(':user_id'=>$pid,':appoint_id'=>$appoint_id);
	$sel_qry="select a.id as appointment_booked_id,appointment_id,doctor_id,note_on_patient,proposed_time,proposed_day,billing_type,billing,fname,lname,profile_pic 
	from appointment_list a,register r where  a.doctor_id=r.id and a.pid=:user_id and a.appointment_id=:appoint_id and a.status='2'";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute($arr);
	$json=$stmt->fetchAll(PDO::FETCH_ASSOC);
	if(!$stmt->rowCount())
	{
		$json['appointment_id']=-2;
		$json['message']="No doctor selected for this appointment";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function chat()
{
	global $pdo;
	extract($_REQUEST);
	$ins_qry="insert into chat(appointment_id,user_id,message,date_time) values(:appointment_id,:user_id,:message,NOW())";
	$stmt_qry=$pdo->prepare($ins_qry);
	$ins_arr=array('appointment_id'=>$appointment_id,':user_id'=>$user_id,':message'=>$message);
	$stmt_qry->execute($ins_arr);
	if($stmt_qry->rowCount())
	{
		$json['id']=1;
		$json['message']="Message sent";
	}
	else
	{
		$json['id']=-2;
		$json['message']="Message not sent";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function return_chat()
{
	global $pdo;
	extract($_REQUEST);
	$arr=array(':appointment_id'=>$appointment_id);
	$sel_qry="select fname,message,date_time from chat c,register r where  c.user_id=r.id and c.appointment_id=:appointment_id";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute($arr);
	$json=$stmt->fetchAll(PDO::FETCH_ASSOC);
	echo "{\"response\":" . json_encode($json) . "}";
}


function return_date_of_appointment()
{
	global $pdo;
	extract($_REQUEST);
	$arr=array(':pid'=>$pid);
	$sel_qry="select id as request_id,CONCAT_WS(' ',final_date_timestamp, from_time) as final_date_timestamp from patient_request where  pid=:pid";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute($arr);
	$json=$stmt->fetchAll(PDO::FETCH_ASSOC);
	if(!$stmt->rowCount())
	{
		$json['request_id']=-2;
		$json['message']="No request done by you";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function cancel_appointment()
{
	global $pdo;
	extract($_REQUEST);
	$arr=array(':appointment_id'=>$appointment_id);
	$del_qry="delete from patient_request where id=:appointment_id";
	$stmt_qry=$pdo->prepare($del_qry);	
	$stmt_qry->execute($arr);	
	if($stmt_qry->rowCount())
	{
		$del_doc_appointments="delete from appointment_list where appointment_id=:appointment_id";
		$del_doc_appointments_qry=$pdo->prepare($del_doc_appointments);	
		$del_doc_appointments_qry->execute($arr);
		$json['id']=1;
		$json['message']="Appointment cancelled";
	}
	else
	{
		$json['id']=-2;
		$json['message']="Appointment not cancelled";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}

function return_edit_current_open_appointment()
{
	global $pdo;
	extract($_REQUEST);
	$arr=array(':appointment_id'=>$appointment_id);
	$sel_qry="select p.quick_desc,p.skill_require,p.street_no,p.location_type,p.street_name,p.suburb,p.pincode,p.state,r.fname,r.lname,r.street_no p_street_no,r.street_name p_street_name,r.phone_no,r.suburb p_suburb,r.pincode p_pincode,r.state p_state from patient_request p,register r where p.pid=r.id and  p.id=:appointment_id";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute($arr);
	$json=$stmt->fetch(PDO::FETCH_ASSOC);
	if(!$stmt->rowCount())
	{
		$json['request_id']=-2;
		$json['message']="Some server error";
	}
	echo "{\"response\":" . json_encode($json) . "}";
}
function update_current_open_appointment()
{
	global $pdo;
	extract($_REQUEST);
	switch($state)
	{
		
		case 'NSW':
			$chk_code=NSW($pincode);
			break;
		case 'ACT':
			$chk_code=ACT($pincode);
			break;
		case 'NT':
			$chk_code=NT($pincode);
			break;
		case 'QLD':
			$chk_code=QLD($pincode);
			break;
		case 'SA':
			$chk_code=SA($pincode);
			break;
		case 'TAS':
			$chk_code=TAS($pincode);
			break;
		case 'VIC':
			$chk_code=VIC($pincode);
			break;
		case 'WA':
			$chk_code=WA($pincode);
			break;
	}
	if($chk_code)
	{
		$updqry="update patient_request set quick_desc=:quick_desc,skill_require=:skills,street_no=:street,street_name=:street_name,suburb=:suburb,state=:state,pincode=:pincode,location_type=:location_type WHERE id=:appointment_id";
		$stmt_qry=$pdo->prepare($updqry);
		$arr=array('appointment_id'=>$appointment_id,':quick_desc'=>$quick_desc,':skills'=>$skills,':street'=>$street,
			':street_name'=>$st_name,':suburb'=>$suburb,':state'=>$state,':location_type'=>$location_type,':pincode'=>$pincode);
		$stmt_qry->execute($arr);
		$json['id']=1;
		$json['message']="Registered Successfully";
	}
	else
	{
		$json['id']=-2;
		$json['message']="Invalid postcode for state";
	}
		
		
	echo "{\"response\":" . json_encode($json) . "}";
}

function truncate()
{
	global $pdo;
	extract($_REQUEST);
	$json=array();
	$arr=array(':table_name'=>$table_name);
	$sel_qry="truncate table :table_name";
	$stmt=$pdo->prepare($sel_qry);
	$stmt->execute($arr);
	echo "{\"response\":" . json_encode($json) . "}";
}


function ACT($postcode)
{
	if(($postcode>=2600 && $postcode<=2618) || ($postcode>=2900 && $postcode<3000))
		return 1;
	else
		return 0;
}

function NSW($postcode)
{
	if(($postcode>=2000 && $postcode<=2599) || ($postcode>=2619 && $postcode<=2899))
		return 1;
	else
		return 0;
}

function NT($postcode)
{
	if($postcode>=800 && $postcode<=999)
		return 1;
	else
		return 0;
}

function QLD($postcode)
{
	if($postcode>=4000 && $postcode<=4999)
		return 1;
	else
		return 0;
}

function SA($postcode)
{
	if($postcode>=5000 && $postcode<=5999)
		return 1;
	else
		return 0;
}

function TAS($postcode)
{
	if($postcode>=7000 && $postcode<=7999)
		return 1;
	else
		return 0;
}

function VIC($postcode)
{
	if($postcode>=3000 && $postcode<=3999)
		return 1;
	else
		return 0;
}

function WA($postcode)
{
	if($postcode>=6000 && $postcode<=6999)
		return 1;
	else
		return 0;
}

?>

